###########################################################
# sTGC
###########################################################

#############
# Output
#############

output top_level {
  output MuonDetectors {
    output STGC {
      output Shifter {
        output Global {
         output OccupancyperLB { 
         }
         output ClusterSizevsSector {
         } 
         output PositionYvsX {
         }
         output ClusterTimevsSector {
         }
        }
        output Occupancy {
         output Strip {
         }
         output Pad {
         }
         output Wire {
         }
        }
        output Lumiblock {
         output Strip {
         }
         output Pad {
         }
         output Wire {
         }
        }
        output Timing {
         output Strip {
         }
         output Pad {
         }
         output Wire {
         }
        }
      }
      output Expert {
        output Summary {
         output ClusterSize {
        }
         output Time {
         }
        }
        output YvsX {
         output Strip {
         }
         output Pad {
         }
         output Wire {
         } 
        } 
    output Charge {
      output Strip {
       output Q1 {
       }
       output Q2 {
       }
       output Q3 {
       }
      }
      output Pad {
       output Q1 {
       }
       output Q2 {
       }
       output Q3 {
       }
      }
      output Wire {
       output Q1 {
       }
       output Q2 {
       }
       output Q3 {
       }
      }
     }
    output Residuals {
      }
     }
    }
   }
 }

#######################
# Histogram Assessments
#######################
dir Muon {

  dir MuonRawDataMonitoring {
    #reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences

    dir sTgc {
      regex = 1
      algorithm = stgc_Histogram_Not_Empty
      dir sTgcLayers {
        dir xyLayeredGlobalPosition { 
          hist xyPos_ASide_* { 
           output = MuonDetectors/STGC/Shifter/Global
           algorithm = stgc_GatherData
          }
         
          hist xyPos_CSide_* {
           output = MuonDetectors/STGC/Shifter/Global
           algorithm = stgc_GatherData
          }
         }
       }

      dir sTgcOccupancy {
         dir stripOccupancy {
           hist stripOccupancy_layer_*[12345678] {
            output = MuonDetectors/STGC/Shifter/Occupancy/Strip
            algorithm = stgc_S_BinsGreaterThanThreshold
            }    
          }
          
         dir padOccupancy {
           hist padOccupancy_layer_1 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pl12_BinsGreaterThanThreshold 
           }
           hist padOccupancy_layer_2 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pl12_BinsGreaterThanThreshold 
           }

           hist padOccupancy_layer_3 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pl34_BinsGreaterThanThreshold
           }
           hist padOccupancy_layer_4 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pl34_BinsGreaterThanThreshold
           }
 
           hist padOccupancy_layer_5 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pl56_BinsGreaterThanThreshold
           }

           hist padOccupancy_layer_6 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pl56_BinsGreaterThanThreshold
           }

           hist padOccupancy_layer_7 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pl78_BinsGreaterThanThreshold
           }
             
           hist padOccupancy_layer_8 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pl78_BinsGreaterThanThreshold
           }  
         }
      
         dir wireGroupOccupancy {
           hist wireGroupOccupancyPerQuad_layer_*[12345678] {
            output = MuonDetectors/STGC/Shifter/Occupancy/Wire
            algorithm = stgc_W_BinsGreaterThanThreshold
           }
         }
       }
      dir sTgcTiming {
         dir padTiming {
           hist padTiming_layer_*[12345678] {
            output = MuonDetectors/STGC/Shifter/Timing/Pad
            algorithm = stgc_Timing
           }
         }
         dir wireGroupTiming {
           hist wireGroupTiming_layer_*[12345678] {
            output = MuonDetectors/STGC/Shifter/Timing/Wire
            algorithm = stgc_Timing
           }
         }
      }
      dir sTgcClusterFromSegment { 
         dir stripClusterTime {
           hist stripClusterTiming_layer_*[12345678] {
            output = MuonDetectors/STGC/Shifter/Timing/Strip
            algorithm = stgc_Timing
           }
           hist stripClusterTiming_layer_*[12345678]@Summary {
            output = MuonDetectors/STGC/Shifter/Global/ClusterTimevsSector
            algorithm = stgc_Timing
           }
         }
         dir stripClusterSize {
           hist stripClusterSize_layer_*[12345678] {
            output = MuonDetectors/STGC/Shifter/Global/ClusterSizevsSector
            algorithm = stgc_Cluster
            display = <AxisRange(0,15,"Y")>,Draw=COL0Z
           }
           hist stripClusterSize_layer_*[12345678]@Summary {
            output = MuonDetectors/STGC/Expert/Summary/ClusterSize
            algorithm = stgc_Cluster
            display = <AxisRange(0,15,"Y")>,Draw=COL0Z
           }
         }
         dir stripChargeSegments { 
           hist stripChargeSegments_CSide_stationEta_1_layer_*[12345678] { 
            output = MuonDetectors/STGC/Expert/Charge/Strip/Q1
            algorithm = stgcStripQ1_landau_Fit
            
           }
         }
      }
      dir sTgcLumiblock {
        dir padSTGClumiblock {
         hist sectorsVersusLumiblockPad_layer_*[12345678] {
          output = MuonDetectors/STGC/Shifter/Lumiblock/Pad
          algorithm = stgc_Histogram_Not_Empty
         }
        }
        dir stripSTGClumiblock {
         hist sectorsVersusLumiblockStrip_layer_*[12345678] {
          output = MuonDetectors/STGC/Shifter/Lumiblock/Strip
          algorithm = stgc_Histogram_Not_Empty
         }
         hist sectorsVersusLumiblockStrip_layer_*[12345678]@Summary {
          output = MuonDetectors/STGC/Shifter/Global/OccupancyperLB
          algorithm = stgc_Histogram_Not_Empty
         }
        } 
        dir wireGroupSTGClumiblock {
         hist sectorsVersusLumiblockWireGroup_layer_*[12345678] {
          output = MuonDetectors/STGC/Shifter/Lumiblock/Wire
          algorithm = stgc_Histogram_Not_Empty
         }
        }
      }
      dir sTgcYvsX {  
       dir YvsXtracksStrip {
        hist sTgcYvsXclusterStrip_ASide_layer_*[12345678] {
         output = MuonDetectors/STGC/Expert/YvsX/Strip
         algorithm = stgc_Histogram_Not_Empty
        }
        hist sTgcYvsXclusterStrip_CSide_layer_*[12345678] {
         output = MuonDetectors/STGC/Expert/YvsX/Strip
         algorithm = stgc_Histogram_Not_Empty
        }
        hist sTgcYvsXclusterStrip_ASide_layer_*[12345678]@Summary {
         output = MuonDetectors/STGC/Shifter/Global/PositionYvsX
         algorithm = stgc_Histogram_Not_Empty
        }
        hist sTgcYvsXclusterStrip_CSide_layer_*[12345678]@Summary {
         output = MuonDetectors/STGC/Shifter/Global/PositionYvsX
         algorithm = stgc_Histogram_Not_Empty
        }
       }
       dir YvsXtracksPad {
        hist sTgcYvsXclusterPad_ASide_layer_*[12345678] {
         output = MuonDetectors/STGC/Expert/YvsX/Pad
         algorithm = stgc_Histogram_Not_Empty
        }

        hist sTgcYvsXclusterPad_CSide_layer_*[12345678] {
         output = MuonDetectors/STGC/Expert/YvsX/Pad
         algorithm = stgc_Histogram_Not_Empty
        }
       }
       dir YvsXtracksWireGroup {
        hist sTgcYvsXclusterWireGroup_ASide_layer_*[12345678] {
         output = MuonDetectors/STGC/Expert/YvsX/Wire
         algorithm = stgc_Histogram_Not_Empty
        }

        hist sTgcYvsXclusterWireGroup_CSide_layer_*[12345678] {
         output = MuonDetectors/STGC/Expert/YvsX/Wire
         algorithm = stgc_Histogram_Not_Empty
        }
       }
      } 
    }
  } 
 }
 
#############
# Algorithms
#############

algorithm stgc_Timing {
  name = NotEmpty&Over&Under&GatherData
  MinStat = 0.9
}

algorithm stgcStripQ1_landau_Fit {
  name = Simple_landau_Fit 
  thresholds = StripQ1_thresholds
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

compositeAlgorithm NotEmpty&Over&Under&GatherData {
  libnames = libdqm_algorithms.so
  subalgs = Histogram_Not_Empty,No_OverFlows,No_UnderFlows,GatherData
}

algorithm stgc_Cluster {
  libname = libdqm_algorithms.so
  name = CheckHisto_Mean
  SubtractFromYMean = 4.0
  thresholds = Cluster_thresholds 
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

algorithm stgc_Histogram_Not_Empty {
  libname = libdqm_algorithms.so
  name = Histogram_Not_Empty
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

algorithm stgc_GatherData {
  name = GatherData
  libname = libdqm_algorithms.so
}

algorithm stgc_Over {
  libname = libdqm_algorithms.so
  name = No_OverFlows
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

algorithm stgc_Under {
  libname = libdqm_algorithms.so
  name = No_UnderFlows
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}


algorithm stgc_S_BinsGreaterThanThreshold {
  libname = libdqm_algorithms.so
  name = Bins_GreaterThan_Threshold 
  BinThreshold = 0.01
  TotalBins = 35280
  thresholds = 	stgc_SEta12_thresholds
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

algorithm stgc_Pl12_BinsGreaterThanThreshold {
  libname = libdqm_algorithms.so
  name = Bins_GreaterThan_Threshold 
  BinThreshold = 0.01
  TotalBins = 5968
  thresholds =  stgc_Pm1s12_thresholds
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

algorithm stgc_Pl34_BinsGreaterThanThreshold {
  libname = libdqm_algorithms.so
  name = Bins_GreaterThan_Threshold 
  BinThreshold = 0.01
  TotalBins = 6656
  thresholds =  stgc_Pm1s34_thresholds
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

algorithm stgc_Pl56_BinsGreaterThanThreshold {
  libname = libdqm_algorithms.so
  name = Bins_GreaterThan_Threshold 
  BinThreshold = 0.01
  TotalBins = 5216
  thresholds =  stgc_Pm2s12_thresholds
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

algorithm stgc_Pl78_BinsGreaterThanThreshold {
  libname = libdqm_algorithms.so
  name = Bins_GreaterThan_Threshold 
  BinThreshold = 0.01
  TotalBins = 5488
  thresholds =  stgc_Pm2s34_thresholds
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

algorithm stgc_W_BinsGreaterThanThreshold {
  libname = libdqm_algorithms.so
  name = Bins_GreaterThan_Threshold  
  BinThreshold = 0.01
  TotalBins = 3588
  thresholds =  stgc_WEta12_thresholds
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}


#############
#thresholds
#############

thresholds stgc_SEta12_thresholds {
  limits NBins {
    warning = 32458
    error = 28224
  }
}

thresholds stgc_Pm1s12_thresholds {
  limits NBins {
    warning =  5490
    error = 4774
  }
}

thresholds stgc_Pm1s34_thresholds {
  limits NBins {
    warning =  6123
    error = 5325
  }
}

thresholds stgc_Pm2s12_thresholds {
  limits NBins {
    warning =  4799
    error = 4173
  }
}

thresholds stgc_Pm2s34_thresholds {
  limits NBins {
    warning =  5049
    error = 4390
  }
}


thresholds stgc_WEta12_thresholds {
  limits NBins {
    warning =  3300
    error = 2870
  }
}

thresholds Cluster_thresholds {
  limits AbsYMean {
   warning = 0.8
   error = 1.0
  }
}

thresholds StripQ1_thresholds { 
  limits MPV {
    warning = 250.0
    error = 150.0
  }
  limits MPV { 
   warning = 320.0 
   error = 350.0 
  }
  limits Sigma { 
   warning = 40.0 
   error = 70.0 
  }
}  
